const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const logger = require('koa-logger')
const favicon = require('koa-favicon')
const userAgent = require('koa-useragent')
const cors = require('koa2-cors')

const index = require('./routes/index')
const transaction = require('./routes/transaction')

// error handler
onerror(app)

// middlewares

app.use(cors({
    origin: function (ctx) {
        let origin = ctx.get('Origin');
        return origin;
        if (process.env.NODE_ENV !== 'production' ||
            origin.endsWith('.58.com')) {
            return origin;
        }
        return false;
    },
    credentials: true
}));

app.use(userAgent);
app.use(favicon(__dirname + '/public/favicon.ico'));


app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}))
app.use(json())
app.use(logger())
app.use(require('koa-static')(__dirname + '/public', {maxage: 2592000000}))

app.use(views(__dirname + '/views', {
    map: {
        html: 'ejs'
    }
}));

// logger
app.use(async (ctx, next) => {
    const start = new Date();
    await next();
    const ms = new Date() - start;
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
})

// routes
app.use(index.routes(), index.allowedMethods())
app.use(transaction.routes(), transaction.allowedMethods())
module.exports = app