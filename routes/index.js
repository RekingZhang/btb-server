const router = require('koa-router')()

router.get('/', async (ctx, next) => {
    if (ctx.userAgent.isMobile) {
        await ctx.render('index.m.html')
        return;
    }
    await ctx.render('index.pc.html')
})

module.exports = router