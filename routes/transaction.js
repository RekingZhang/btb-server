const router = require('koa-router')();
var Tx = require('ethereumjs-tx');
const privateKey =
    '3B144CF2D338D30FF02994A1D934B56A51F1B2C94B4420AA288D7DB8C7DC7215';

router.post('/transaction', async (ctx, next) => {
    const params = ctx.request.body;
    const transaction = new Tx(params);
    transaction.sign(Buffer.from(privateKey, 'hex'));
    ctx.body = {
        hash: transaction.serialize().toString('hex')
    }
})

module.exports = router
